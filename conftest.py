import pytest
from django_features.models import State


@pytest.fixture
def public_state():
    state = State(name='public-state', opt_in=False, public=True)
    state.save()
    return state
