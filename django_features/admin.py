from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django_features.models import (AbstractFeatureLink,
                                    Feature, FeatureLink,
                                    ContentFeature, ContentFeatureLink,
                                    State, StateGroupMembership)

def create_name_field(model_class):
    return forms.ChoiceField(
        choices=[(None, '')] +
                [(name, name) for name in model_class.link_settings().keys()])

class FeatureLinkFormMixin(object):
    def __init__(self, *args, **kwargs):
        super(FeatureLinkFormMixin, self).__init__(*args, **kwargs)
        self.fields['text'].required = False

    def clean_name(self):
        name = self.cleaned_data['name']
        link_settings = AbstractFeatureLink.link_settings()
        if name not in link_settings:
            raise ValidationError('valid link names: %r' %
                                  link_settings.keys())
        return name

    def clean_text(self):
        if self.cleaned_data['text']:
            return self.cleaned_data['text']

        name = self.cleaned_data.get('name')
        link_settings = AbstractFeatureLink.link_settings()
        if 'text' not in link_settings.get(name, {}):
            raise ValidationError('text is required')
        return link_settings[name]['text']


class FeatureLinkForm(FeatureLinkFormMixin, forms.ModelForm):
    class Meta:
        model = FeatureLink
        fields = '__all__'

    name = create_name_field(FeatureLink)


class ContentFeatureLinkForm(FeatureLinkFormMixin, forms.ModelForm):
    class Meta:
        model = ContentFeatureLink
        fields = '__all__'

    name = create_name_field(ContentFeatureLink)


class FeatureLinkInline(admin.TabularInline):
    model = FeatureLink
    form = FeatureLinkForm
    extra = 3
    verbose_name = 'link'


class ContentFeatureLinkInline(admin.TabularInline):
    model = ContentFeatureLink
    form = ContentFeatureLinkForm
    extra = 3
    verbose_name = 'link'


class FeatureAdmin(admin.ModelAdmin):
    list_display = ('slug', 'state', 'summary')
    readonly_fields = ('updated_on', 'created_on')
    fieldsets = (
        (None, {
            'fields': ('slug', 'summary', 'created_on', 'updated_on')
        }),
        ('Promotion', {
            'fields': ('promote', 'promotion_date', 'description',
                       'screenshot', 'tweet')
        }),
        ('Release', {
            'fields': ('state', 'partial_rollout', 'percentage')
        }),
        ('Life cycle', {
            'fields': ('owner', 'owner_email', 'expiration_date')
        }),
    )
    inlines = (FeatureLinkInline,)


class ContentFeatureAdmin(admin.ModelAdmin):
    list_display = ('slug', 'summary')
    readonly_fields = ('updated_on', 'created_on')
    fieldsets = (
        (None, {
            'fields': ('slug', 'summary', 'created_on', 'updated_on')
        }),
        ('Promotion', {
            'fields': ('promote', 'promotion_date', 'description',
                       'screenshot', 'tweet')
        }),
        ('Release', {
            'fields': ('partial_rollout', 'percentage')
        }),
        ('Life cycle', {
            'fields': ('owner', 'owner_email', 'expiration_date')
        }),
    )
    inlines = (ContentFeatureLinkInline,)


class StateGroupMembershipInline(admin.TabularInline):
    model = StateGroupMembership
    extra = 0
    verbose_name = 'Group'


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'public', 'opt_in', 'label',)
    inlines = (StateGroupMembershipInline,)


admin.site.register(Feature, FeatureAdmin)
admin.site.register(ContentFeature, ContentFeatureAdmin)
admin.site.register(State, StateAdmin)
