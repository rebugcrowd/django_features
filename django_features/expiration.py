from datetime import datetime
import logging
from django.conf import settings
from django.utils.module_loading import import_string

logger = logging.getLogger(__name__)


def _default_expired_callback(feature):
    expiration_date = "{:%Y-%m-%d %H:%M:%S}".format(feature.expiration_date)
    msg = "Feature '%s' expired at %s." % (feature.slug, expiration_date)
    if feature.owner or feature.owner_email:
        msg += " Contact owner:"
    if feature.owner:
        msg += " %s" % feature.owner
    if feature.owner_email:
        msg += " <%s>" % feature.owner_email
    logger.warning(msg)


class _CheckExpiration(object):
    def __init__(self):
        self.expired_callback = _default_expired_callback
        if hasattr(settings, 'DJANGO_FEATURE_EXPIRED'):
            try:
                if isinstance(settings.DJANGO_FEATURE_EXPIRED, str):
                    self.expired_callback = import_string(
                        settings.DJANGO_FEATURE_EXPIRED)
                else:
                    self.expired_callback = settings.DJANGO_FEATURE_EXPIRED
            except (ImportError):
                logger.warning("Cannot import '%s'. "
                    "Check DJANGO_FEATURE_EXPIRED in settings." %
                    settings.DJANGO_FEATURE_EXPIRED)

    def __call__(self, feature):
        if (feature.expiration_date is not None and
                datetime.now() >= feature.expiration_date):
            self.expired_callback(feature)


check_expiration = _CheckExpiration()
