import uuid
import threading
from functools import wraps

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.core.cache import caches
from django.core.signals import request_started
from django.dispatch.dispatcher import receiver
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.http.response import Http404
from django_features.models import (ContentFeature, Feature, FeatureFlag,
                                    State, StateGroupMembership, FeatureLink)

_feature_set_cache_key_template = 'features:%s:active:%d'
_feature_key_cache_key = 'features:key'
_user_attribute_name = '_feature_set'

_local_cache = threading.local()


class FeatureSet(object):
    _cache = None
    _feature_key = None
    _cache_key = None

    _feature_set = None

    def __init__(self, user):
        self._uid = user.id if user.is_authenticated else -1
        self._user_instances = {}
        self._add_user_instance(user)
        setattr(user, _user_attribute_name, self)

    def _add_user_instance(self, user):
        """
        There can be multiple instances of a given user in a given thread.
        """
        self._user_instances[id(user)] = user

    @staticmethod
    def for_user(user):
        feature_set = getattr(user, _user_attribute_name, None)
        if feature_set:
            return feature_set

        # If not running in a django request the threadlocal cache is disabled.
        featuresets = _local_cache.__dict__.get('featuresets')
        if featuresets is not None:
            user_id = user.id if user.is_authenticated else -1
            feature_set = featuresets.get(user_id)

            if feature_set:
                feature_set._add_user_instance(user)
                setattr(user, _user_attribute_name, feature_set)
                return feature_set

            feature_set = FeatureSet(user)
            featuresets[user_id] = feature_set
            return feature_set

        return FeatureSet(user)

    @staticmethod
    def get_key_cache():
        """ Get the cache used for storing the root feature key.

        The DJANGO_FEATURES_KEY_CACHE setting can be defined to specify this
        cache explicitly, if using a different cache from the one used to store
        feature sets for a user. Otherwise this will fall back to using the
        same cache specified by DJANGO_FEATURES_CACHE.
        """
        return caches[getattr(settings, 'DJANGO_FEATURES_KEY_CACHE',
                              getattr(settings, 'DJANGO_FEATURES_CACHE',
                                      'default'))]

    @staticmethod
    def get_features_cache():
        return caches[getattr(settings, 'DJANGO_FEATURES_CACHE', 'default')]

    @staticmethod
    def update_feature_key():
        cache = FeatureSet.get_key_cache()
        cache.set(_feature_key_cache_key, uuid.uuid4().hex, version=1)

    @property
    def cache(self):
        if not self._cache:
            self._cache = FeatureSet.get_features_cache()
        return self._cache

    @property
    def feature_key(self):
        """ Root feature key

        This key enables the expiration of every cache by changing the prefix
        """
        if not self._feature_key:
            self._feature_key = self.get_key_cache().get(
                _feature_key_cache_key, 'none', version=1)
        return self._feature_key

    @property
    def cache_key(self):
        return _feature_set_cache_key_template % (self.feature_key, self._uid)

    @property
    def feature_set(self):
        """ Returns a set of all features available to a user as a dict

        The key of the dict is the slug of the feature, the value is whether the
        user has it enabled.
        """
        if not self._feature_set:
            cache_key = self.cache_key

            features = self.cache.get(cache_key, version=1)

            if not features:
                features = Feature.objects.feature_enablement_for_user(
                    list(self._user_instances.values())[0]
                )
                self.cache.set(
                        cache_key,
                        features,
                        900,
                        version=1)

            self._feature_set = features

        return self._feature_set

    def has_feature(self, name):
        return self.feature_set.get(name, False)

    def clear(self):
        for u in self._user_instances.values():
            delattr(u, _user_attribute_name)
        self.cache.delete(self.cache_key, version=1)

        featuresets = _local_cache.__dict__.get('featuresets')
        if featuresets:
            featuresets.pop(self._uid, None)

    def get(self, item, default=None):
        return self.feature_set.get(item, default)

    def __getitem__(self, item):
        return self.feature_set[item]

    def __contains__(self, item):
        return item in self.feature_set

    def __iter__(self):
        return self.feature_set.__iter__()

    def iterkeys(self):
        return self.feature_set.iterkeys(self)

    def enabled(self):
        return {slug for slug, enabled in self.feature_set.items() if enabled}


def has_feature(user, name):
    if isinstance(user, (get_user_model(), AnonymousUser)):
        return FeatureSet.for_user(user).has_feature(name)

    content = user
    return ContentFeature.objects.get(slug=name).active(content)


def _clear_feature_set_cache(sender, instance=None, **kwargs):
    user = instance.user
    FeatureSet.for_user(user).clear()


def _set_feature_key(sender, instance=None, **kwargs):
    FeatureSet.update_feature_key()


post_save.connect(_set_feature_key, sender=Feature,
                  dispatch_uid='update_feature_key_on_save')
post_delete.connect(_set_feature_key, sender=Feature,
                    dispatch_uid='update_feature_key_on_delete')
post_save.connect(_set_feature_key, sender=State,
                  dispatch_uid='update_feature_key_on_state_change')


post_delete.connect(_set_feature_key, sender=FeatureLink,
                    dispatch_uid='update_feature_key_on_link_delete')
post_save.connect(_set_feature_key, sender=FeatureLink,
                  dispatch_uid='update_feature_key_on_link_change')

post_delete.connect(_set_feature_key, sender=StateGroupMembership,
                    dispatch_uid='update_feature_key_on_state_group_delete')
post_save.connect(_set_feature_key, sender=StateGroupMembership,
                  dispatch_uid='update_feature_key_on_state_group_change')

post_save.connect(_clear_feature_set_cache, sender=FeatureFlag,
                  dispatch_uid='clear_feature_set_cache_on_save')
post_delete.connect(_clear_feature_set_cache, sender=FeatureFlag,
                    dispatch_uid='clear_feature_set_cache_on_delete')


@receiver(request_started)
def clear_threadlocal(sender, **kwargs):
    _local_cache.featuresets = {}


def has_feature_or_404(feature_name, get_user=None):
    def decorator(func):
        @wraps(func)
        def wrapped(request, *args, **kwargs):
            user = get_user(request) if get_user is not None else request.user
            if not has_feature(user, feature_name):
                raise Http404
            return func(request, *args, **kwargs)
        return wrapped
    return decorator
