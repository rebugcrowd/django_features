from django_features.utils import _local_cache


def pytest_runtest_setup(item):
    _local_cache.featuresets = {}
