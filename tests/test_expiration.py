from datetime import datetime, timedelta
import mock
import pytest
from django.contrib.auth.models import User
from django.test import override_settings
from django_features import expiration
from django_features.models import Feature, State
from django_features.utils import FeatureSet

EXPECTED_MSG = "Feature 'expired_feature' expired at 1969-07-20 20:18:00."
mock_expired_callback = mock.MagicMock()


@pytest.fixture
def user1():
    user = User(username='user1')
    user.set_password('')
    user.save()
    return user


@pytest.fixture
def user2():
    user = User(username='user2')
    user.set_password('')
    user.save()
    return user


@pytest.fixture
def current_feature(public_state):
    in_the_future = datetime.now() + timedelta(days=1)
    feature = Feature(slug='current_feature', summary='current',
                      state=public_state, expiration_date=in_the_future)
    feature.save()
    return feature


@pytest.fixture
def expired_feature(public_state):
    in_the_past = datetime(1969, 7, 20, 20, 18, 00)  # Apollo 11
    feature = Feature(slug='expired_feature', summary='expired',
                      state=public_state, expiration_date=in_the_past)
    feature.save()
    return feature


@pytest.fixture
def never_expires_feature(public_state):
    feature = Feature(slug='never_expires', summary='no expiration date',
                      state=public_state, expiration_date=None)
    feature.save()
    return feature


@pytest.mark.django_db
def test_check_expiration_with_expired_feature(expired_feature):
    mock_expired_callback.reset_mock()

    # Work around pytest bug with Python 2.7 where fixture is not passed
    with override_settings(DJANGO_FEATURE_EXPIRED=mock_expired_callback):

        # Use patch as context manager to pick up the overridden setting
        with mock.patch('django_features.models.check_expiration',
                        expiration._CheckExpiration()) as check_expiration:
            check_expiration(expired_feature)
            mock_expired_callback.assert_called_once_with(expired_feature)


@pytest.mark.django_db
def test_check_expiration_with_current_feature(current_feature):
    mock_expired_callback.reset_mock()

    # Work around pytest bug with Python 2.7 where fixture is not passed
    with override_settings(DJANGO_FEATURE_EXPIRED=mock_expired_callback):

        # Use patch as context manager to pick up the overridden setting
        with mock.patch('django_features.models.check_expiration',
                        expiration._CheckExpiration()) as check_expiration:
            check_expiration(current_feature)
            mock_expired_callback.assert_not_called()


@pytest.mark.django_db
def test_check_expiration_with_no_expiration_date(never_expires_feature):
    mock_expired_callback.reset_mock()

    # Work around pytest bug with Python 2.7 where fixture is not passed
    with override_settings(DJANGO_FEATURE_EXPIRED=mock_expired_callback):

        # Use patch as context manager to pick up the overridden setting
        with mock.patch('django_features.models.check_expiration',
                        expiration._CheckExpiration()) as check_expiration:
            check_expiration(never_expires_feature)
            mock_expired_callback.assert_not_called()


@mock.patch('django_features.models.check_expiration')
@pytest.mark.django_db
def test_check_expiration_called(mock_check_expiration, user1,
                                 current_feature):
    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(current_feature.slug)
    mock_check_expiration.assert_called_once_with(current_feature)
    mock_check_expiration.reset_mock()

    FeatureSet.for_user(user1).has_feature(current_feature.slug)
    # check_expiration() is not called when reading from cache
    mock_check_expiration.assert_not_called()

    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(current_feature.slug)
    mock_check_expiration.assert_called_once_with(current_feature)


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_default_expired_callback(mock_logger, expired_feature):
    # No owner
    expiration.check_expiration(expired_feature)
    mock_logger.warning.assert_called_once_with(EXPECTED_MSG)
    mock_logger.reset_mock()

    # Owner name only
    expired_feature.owner = 'Neil Armstrong'
    expired_feature.save()
    expiration.check_expiration(expired_feature)
    mock_logger.warning.assert_called_once_with(
        EXPECTED_MSG + ' Contact owner: Neil Armstrong')
    mock_logger.reset_mock()

    # Owner name and email
    expired_feature.owner_email = 'na@nasa.gov'
    expired_feature.save()
    expiration.check_expiration(expired_feature)
    mock_logger.warning.assert_called_once_with(
        EXPECTED_MSG + ' Contact owner: Neil Armstrong <na@nasa.gov>')
    mock_logger.reset_mock()

    # Owner email only
    expired_feature.owner = None
    expired_feature.save()
    expiration.check_expiration(expired_feature)
    mock_logger.warning.assert_called_once_with(
        EXPECTED_MSG + ' Contact owner: <na@nasa.gov>')


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_default_callback_with_cache(mock_logger, user1, expired_feature):
    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(expired_feature.slug)
    mock_logger.warning.assert_called_once_with(EXPECTED_MSG)
    mock_logger.reset_mock()

    FeatureSet.for_user(user1).has_feature(
        expired_feature.slug)
    mock_logger.warning.assert_not_called() # read from cache

    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(expired_feature.slug)
    mock_logger.warning.assert_called_once_with(EXPECTED_MSG)


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_default_callback_never_expires(mock_logger, user1, never_expires_feature):
    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(never_expires_feature.slug)
    mock_logger.warning.assert_not_called()


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_default_callback_not_expired(mock_logger, user1, current_feature):
    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user1).has_feature(current_feature.slug)
    mock_logger.warning.assert_not_called()


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_invalid_function_expired_callback(mock_logger, user1, expired_feature):
    # Work around pytest bug with Python 2.7 where fixture is not passed
    with override_settings(
            DJANGO_FEATURE_EXPIRED='tests.test_features.DOES_NOT_EXIST'):

        # Use patch as context manager to pick up the overridden setting
        with mock.patch('django_features.models.check_expiration',
                        expiration._CheckExpiration()) as check_expiration:

            FeatureSet.for_user(user1).clear()
            FeatureSet.for_user(user1).has_feature(expired_feature.slug)
            mock_logger.warning.assert_has_calls([
                mock.call("Cannot import 'tests.test_features.DOES_NOT_EXIST'."
                          " Check DJANGO_FEATURE_EXPIRED in settings."),
                mock.call("Feature 'expired_feature' expired at "
                          "1969-07-20 20:18:00.")])


@mock.patch('django_features.expiration.logger')
@pytest.mark.django_db
def test_expired_callback_per_user(mock_logger, user1, user2, expired_feature):
    """ An expired feature callback is called for each cache miss.
        This may be undesired behavior.

        Features are cached per user. This means expired features will
        generate a lot of noise and may even cause a performance slowdown
        under high load.
    """
    FeatureSet.for_user(user1).clear()
    FeatureSet.for_user(user2).clear()

    FeatureSet.for_user(user1).has_feature(expired_feature.slug)
    mock_logger.warning.assert_called_once_with(EXPECTED_MSG)

    mock_logger.reset_mock()
    FeatureSet.for_user(user2).has_feature(expired_feature.slug)
    mock_logger.warning.assert_called_once_with(EXPECTED_MSG)
