import pytest

from django.utils.cache import caches
from django.contrib.auth.models import User
from django.http.response import Http404

from django_features.models import Feature, State
from django_features.utils import has_feature_or_404, _feature_key_cache_key


def make_user(username):
    u = User(username=username)
    u.set_password('')
    u.save()
    return u


@pytest.fixture
def user():
    return make_user('u')


@pytest.fixture
def target_user():
    return make_user('t')


@pytest.fixture
def django_request(rf, user, target_user):
    req = rf.get('/')
    req.user = user
    req.target_user = target_user
    return req


@pytest.fixture
def feature():
    state = State(name='test_opt_in', opt_in=True, public=True)
    state.save()
    feature = Feature(slug='test_feature', state=state)
    feature.save()
    return feature


@pytest.mark.parametrize('with_feature', [True, False])
@pytest.mark.django_db
def test_has_feature_decorator_default(with_feature, django_request, feature, user):
    @has_feature_or_404(feature.slug)
    def method(request):
        return True

    if with_feature:
        feature.enable(user)
        assert method(django_request)
    else:
        with pytest.raises(Http404):
            method(django_request)


@pytest.mark.parametrize('with_feature', [True, False])
@pytest.mark.django_db
def test_has_feature_decorator_with_custom_user(with_feature, django_request, feature, target_user):
    @has_feature_or_404(feature.slug, get_user=lambda r: r.target_user)
    def method(request):
        return True

    if with_feature:
        feature.enable(target_user)
        assert method(django_request)
    else:
        with pytest.raises(Http404):
            method(django_request)

@pytest.mark.django_db
def test_cache_separation(django_request, feature, target_user):
    feature.slug = 'arbitrary-change'
    feature.save()

    assert caches['default'].get(_feature_key_cache_key, None) is None
    assert caches['features'].get(_feature_key_cache_key, None) is None
    assert caches['feature-key'].get(_feature_key_cache_key, None) is not None
